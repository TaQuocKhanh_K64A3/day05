<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Xác nhận</title>
</head>
<style>
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: auto;
    margin-top: 2rem;
  }

  .form-group {
    width: 30vw;
    height: inherit;
    background-color: white;
    padding: 2rem;
    display: flex;
    flex-direction: column;
    border: 2px solid #4f85b4;
  }

  .form-child {
    height: 36px;
    margin: 10px 0px;
    display: flex;
    align-items: center;
    justify-content: space-around;
  }

  .form-text {
    background-color: #70ad46;
    height: inherit;
    width: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0px 6px;
    border: 2px solid #4f85b4;
    color: white;
  }

  .sub-form-text {
    height: inherit;
    width: 200px;
    padding: 0px;
    display: flex;
    align-items: center;

  }

  .form-radio {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px;
  }

  .form-select {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px
  }

  .form-birthday {
    display: flex;
    align-items: center;
    height: inherit;
    width: 160px;
    margin-right: 40px;
    padding: 0px 0px 0px 10px;
    box-sizing: border-box;
    border: 2px solid #4f85b4;
  }

  .sub-form-image {
    width: 200px;
    padding: 0px;
  }

  .form-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 2rem;
  }

  .btn-submit {
    height: 44px;
    width: 140px;
    border-radius: 5px;
    border: 2px solid #4f85b4;
    background-color: #70ad46;
    color: white;
  }
</style>

<body>
  <?php
  session_start();
  $fullName = isset($_SESSION['data']['fullName']) ? $_SESSION['data']['fullName'] : "";
  $gender = isset($_SESSION['data']['gender']) ? $_SESSION['data']['gender'] : "";
  $khoa = isset($_SESSION['data']['khoa']) ? $_SESSION['data']['khoa'] : "";
  $birthday = isset($_SESSION['data']['birthday']) ? $_SESSION['data']['birthday'] : "";
  $address = isset($_SESSION['data']['address']) ? $_SESSION['data']['address'] : "";
  $image = isset($_SESSION['image']) ? $_SESSION['image'] : "";
  ?>
  <div class="container">
    <div class="form-group">
      <div class="form-child">
        <label class="form-text">
          Họ và tên
        </label>
        <div class="sub-form-text">
          <?php
          echo $fullName;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-text">
          Giới tính
        </label>
        <div class="sub-form-text">
          <?php
          echo $gender;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-text">
          Phân khoa
        </label>
        <div class="sub-form-text">
          <?php
          $khoaArray = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
          foreach ($khoaArray as $key => $value) {
            if ($key == $khoa) {
              echo $value;
            }
          }
          ?>
          </select>
        </div>
      </div>

      <div class="form-child" date-date-format="dd/MM/yyyy">
        <label class="form-text">
          Ngày sinh
        </label>
        <div class="sub-form-text">
          <?php
          echo $birthday;
          ?>
          </select>
        </div>
      </div>

      <div class="form-child">
        <label class="form-text">
          Địa chỉ
        </label>
        <div class="sub-form-text">
          <?php
          echo $address;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-text">
          Hình ảnh
        </label>
        <div class="sub-form-text">
          <?php
          echo '<span class="form-control1"><img src="' . $image . '" height="50px" width="50px"></span>'
          ?>
        </div>
      </div>
      <div class="form-btn">
        <input type="button" name="btnSubmit" id="btnSubmit" class="btn-submit" value="Xác nhận">
      </div>
    </div>
  </div>
</body>

</html>